*** Settings ***
Library  RequestsLibrary

*** Variables ***
${URL_GET}    http://demo.efinancethai.com/smartws/service.asmx
${URL_POST}   http://demo.efinancethai.com/smartws/service.asmx
${symbol}  KBANK

*** Keywords ***
Get API EFIN TEST
    Create Session    EFINAPI    ${URL_GET}
    ${resp}=  Get Request    EFINAPI    /HelloWorld
    Log To Console  ${\n}${resp.text}
    #BuiltIn.Should Contain  ${resp.text}  <?xml version="1.0" encoding="utf-8"?>
    BuiltIn.Should Be Equal As Integers  ${resp.status_code}  200
    BuiltIn.Should Contain  ${resp.text}  Soonthorn

Get API EFIN TEST2
    Create Session    EFINAPI    ${URL_GET}
    &{data}=  Create Dictionary  nameStr=${symbol}
    ${resp}=  Get Request    EFINAPI    /SendData  params=&{data}
    Log To Console  ${\n}${resp.text}
    #BuiltIn.Should Contain  ${resp.text}  <?xml version="1.0" encoding="utf-8"?>
    BuiltIn.Should Be Equal As Integers  ${resp.status_code}  200
    BuiltIn.Should Contain  ${resp.text}  ${symbol}

Get API EFIN TEST3
    Create Session    EFINAPI    ${URL_GET} 
    &{data}=  Create Dictionary  Key=@eft  Cmd=005  Param=BBL
    ${resp}=  Get Request    EFINAPI    /ReturnValue  params=&{data}
    Log To Console  ${\n}${resp.text}
    #BuiltIn.Should Contain  ${resp.text}  <?xml version="1.0" encoding="utf-8"?>
    BuiltIn.Should Be Equal As Integers  ${resp.status_code}  200
    #BuiltIn.Should Contain  ${resp.text}  ${symbol}

POST API EFIN TEST
    Create Session    EFIN    ${URL_POST}
    &{data}=  Create Dictionary   
    &{headers}=  Create Dictionary    Content-Type=text/xml
    ${resp}=  POST Request    EFIN    /HelloWorld  headers=${headers}
    Log To Console  ${\n}${resp.text}
    BuiltIn.Should Be Equal As Integers  ${resp.status_code}   200
    BuiltIn.Should Contain  ${resp.text}  Soonthorn

POST API EFIN TEST2
    Create Session    EFIN    ${URL_POST}
    &{data}=  Create Dictionary  nameStr=${symbol}  Content-Type=application/json  charset=utf-8
    ${resp}=  POST Request    EFIN    /SendData   params=${data}  headers=${data} 
    #${resp}=  Get Request    EFINAPI    /SendData  params=&{data}
    Log To Console  ${\n}${resp.text}
    #BuiltIn.Should Be Equal As Integers  ${resp.status_code}  200
    #BuiltIn.Should Contain ${resp.text}  Soonthorn

POST API EFIN TEST3
    Create Session    EFIN    ${URL_POST}
    &{data}=  Create Dictionary  Key=@eft  Cmd=005  Param=BBL
    &{headers}=  Create Dictionary   Content-Type=application/json    authorisationFlag=N  Accept=*/*  Cache-Control=no-cache
    #${resp}=  POST Request    EFIN    /ReturnValue   params=${data}  headers=${headers}
    ${resp}=  Post Request  EFIN   /ReturnValue/    data=${data}    headers=${headers}  
    Log To Console  ${\n}${resp.text}
    #BuiltIn.Should Be Equal As Integers  ${resp.status_code}   200
    #BuiltIn.Should Contain  ${resp.text}  Soonthorn



*** Test Cases ***
#Test Get API EFIN
    #Get API EFIN TEST
#Test Get API EFIN2
    #Get API EFIN TEST2
Test Get API EFIN3
    Get API EFIN TEST3
#Test POST API EFIN
  #POST API EFIN TEST
#Test POST API EFIN2
   #POST API EFIN TEST2
#Test POST API EFIN3
   #POST API EFIN TEST3

