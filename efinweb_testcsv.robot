*** Setting ***
Documentation     Test login broker Intra + Inter
Library  SeleniumLibrary
#Library  AlertMail.py
Library  CSVLib.py

*** Variables ***
#${user_name}  
#${user_pwd}  
${users}

*** Test Cases ***
Login users from CSV
    Get users CSV
    FOR  ${user}  IN  @{users}
        Login Web   ${user}[1]  ${user}[2]
        Check login success  ${user}[1]
    END
 

*** Keywords ***
Get users CSV
    ${users}=  Read file to list  userefin.csv
    Log To Console  ${users}
    Set Global Variable  ${users} 

Login Web
    [Arguments]  ${user_name}  ${user_pwd}
    Open Browser  https://www.efinancethai.com/index.aspx
    ...  browser=chrome
    ...  remote_url=http://localhost:4444/wd/hub
    #Title Should Be  สรุปข่าวหุ้น ข่าวเศรษฐกิจ การเงินและการลงทุน  | efinanceThai
    Maximize Browser Window
    #Wait Until Element Is Visible  //*[@id="btnBrokeLogin"]
    Wait Until Page Contains Element  //*[@id="btnBrokeLogin"]
    Input Text  brokeruser  ${user_name}
    Input Text  brokerpass  ${user_pwd}
    Click Button  //*[@id="btnBrokeLogin"]
Check login success
    [Arguments]  ${user_name}
    Log To Console  Wait login success
    #Wait Until Element Is Visible  //*[@id="rowpadding"]/div/div[1]/div[1]/div[2]/span
    Register Keyword To Run On Failure  Nothing
    ${status}=  Run Keyword and Ignore Error  Wait Until Page Contains Element  //*[@id="rowpadding"]/div/div[1]/div[1]/div[2]/span  timeout=3
    ${response}=  Run Keyword If  ${status} == 'PASS'
    ...  Get Text  //*[@id="rowpadding"]/div/div[1]/div[1]/div[2]/span
    ...  ELSE  Get Text  //*[@id="rowpadding"]/div/div[1]/div[1]/div[2] 
    
    #${result}=  Run Keyword If  ${status} == 'FAIL'  send_email_from_testcase  ${user_name}  ${response}
    #send_email_from_testcase  ${user_name}  ${response}

    #${result}=  Run Keyword If  ${status} == 'FAIL'  send_email_from_testcase  'can not login'  ${response}
  

    #Should Be True  ${response} == ${user_name}
    #Should Be True  1 == 1
    #${response}=  Run Keyword If  ${status} == 'FAIL'
    #...  Get Text  //*[@id="divLoginAlert"]
    #Wait Until Page Contains Element //*[@id="rowpadding"]/div/div[1]/div[1]/div[2]/span  timeout=3
    #Run Keyword If  '${status}' == 'FAIL'  Get Text  //*[@id="rowpadding"]/div/div[1]/div[1]/div[2]/span
    #Validate  ${response}  ${user_name}
    Log To Console   ${user_name} + ' ' + ${response}
    Close Browser