*** Setting ***
Library  SeleniumLibrary
Test Teardown  Close Browser

*** Test Cases ***
Try
    Open Browser  https://tympanus.net/Development/PageLoadingEffects/
    ...  browser=chrome
    ...  remote_url=http://localhost:4444/wd/hub
    #Execute JavaScript    document.getElementById("element-id").onclick()
    #Click Link  //*[@id="page-1"]/section[1]/div[2]/p[2]/a

    Click Link  Show Page Loader

    Wait Until Element Is Visible  //*[@id="loader"]
    Wait Until Element Is Not Visible  //*[@id="loader"]
    #Wait Until Page Contains Element  //*[@id="page-2"]/section/p/a

    Wait Until Element Is Visible  //*[@id="page-2"]/section/p/a
    Click Link  //*[@id="page-2"]/section/p/a

