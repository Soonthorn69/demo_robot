*** Setting ***
Library  SeleniumLibrary
Library  Screenshot
Library  DateTime	
Test Teardown  Close Browser

*** Test Cases ***
Try Open Browser
    Open Browser  https://www.google.com
    ...  browser=chrome
    ...  remote_url=http://localhost:4444/wd/hub
    Screenshot  test.png

*** Keywords ***
Screenshot
   [Arguments]  ${filename}
   SeleniumLibrary.Set Screenshot Directory  my_screen_shot
   Wait Until Page Contains  Element
   ${date1}=  Get Current Date  result_format=%Y-%m-%d %H-%M-%S
  
   Capture Page Screenshot  ${date1} + ${filename}
   Log To Console  ${\n}Screenshot + ${date1}