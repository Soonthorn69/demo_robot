*** Setting ***
Documentation     Test login broker Intra + Inter
Library  SeleniumLibrary
#Test Teardown  Close Browser

*** Variables ***
${user_name}    98-soonthorn
${user_pwd}    123456

*** Test Cases ***
Login Web
    Open Browser  https://www.efinancethai.com/index.aspx
    ...  browser=chrome
    ...  remote_url=http://localhost:4444/wd/hub
    #Title Should Be  สรุปข่าวหุ้น ข่าวเศรษฐกิจ การเงินและการลงทุน  | efinanceThai
    Wait Until Element Is Visible  //*[@id="btnBrokeLogin"]
    Input Text  brokeruser  ${user_name}
    Input Text  brokerpass  ${user_pwd}
    Click Button  //*[@id="btnBrokeLogin"]
Check login success
    Log To Console  Wait login success
    Wait Until Element Is Visible  //*[@id="rowpadding"]/div/div[1]/div[1]/div[2]/span
    ${response}  Get Text  //*[@id="rowpadding"]/div/div[1]/div[1]/div[2]/span
    #Validate  ${response}  ${user_name}
    Log To Console   ${response}
    Close Browser