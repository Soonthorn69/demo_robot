*** Setting ***
Library  SeleniumLibrary
#Test Teardown  Close Browser

*** Test Cases ***
Try Open Browser
    Open Browser  https://robotframework.org/
    ...  browser=chrome
    ...  remote_url=http://localhost:4444/wd/hub
    Click Link  web demo project
    @{windows}  Get Window Handles
    Log to Console  ${windows}[0]
    Log to Console  ${windows}[1]
    Select Window  ${windows}[0]
    Select Window  ${windows}[1]

    Close Browser